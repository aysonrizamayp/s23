

let pokemons = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
				hoenn: ["May", "Max"],				
				kanto: ["Brock", "Misty"]
			 },
			 talk: function() {
			 	console.log("Pikachu! I choose you!");
			 }
}
console.log(pokemons);

console.log("Result of dot notation:");
console.log(pokemons.name);

console.log("Result of square bracket notation:");
console.log(pokemons["pokemon"]);

console.log("Result of talk method:");
pokemons.talk();



function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		let newHealth = target.health - this.attack

		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(newHealth));

		if(newHealth <= 0){
			target.faint()
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(mewtwo)
console.log(pikachu)
console.log(geodude)

geodude.tackle(pikachu);
mewtwo.tackle(geodude);






